package org.honor.springmvc.javaconfig.example1.controller;

import java.util.List;

import org.honor.springmvc.javaconfig.example1.domain.Organization;
import org.honor.springmvc.javaconfig.example1.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OrganizationController {
	
	@Autowired
	private OrganizationService organizationService;
	
	@RequestMapping("/location")
	public String addLocation() {
		return "location";
	}
	
	@RequestMapping("/listOrgs")
	public String listOrganizations(Model model) {
		List<Organization> orgList = organizationService.getOrgList();
		model.addAttribute("orgList", orgList);
		return "listOrganizations";
	}
}
