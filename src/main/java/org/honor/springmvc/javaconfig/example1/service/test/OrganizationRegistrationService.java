package org.honor.springmvc.javaconfig.example1.service.test;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OrganizationRegistrationService {

	/*
	 * map to the property bean defined in WebMvcConfig.java using @Value annotation
	 * */
	@Value("#{ageOptions}")
	private Map<String, String> ageList;
	/*
	 * we use LinkedHashMap so that the values we add appear in the same order as we added them
	 * */
	public Map<String, String> populateTurnover(){
		Map<String,String> turnover = new LinkedHashMap<String,String>();
		turnover.put("none", "-----select-----");
		turnover.put("100", "less than 100");
		turnover.put("1000", "more than 100 less than 1,000");
		turnover.put("10000", "more than 1,000 less than 10,000");
		return turnover;
	}
	
	public Map<String, String> populateTypes(){
		Map<String,String> types = new LinkedHashMap<String,String>();
		types.put("gov", "government");
		types.put("semi-gov", "semi government");
		types.put("private", "private");
		return types;
	}

	/*
	 * use TreeMap to ensure the natural ordering of the select options 
	 * according to the key values defined in the properties file.
	 * */
	public Map<String, String> populateServiceAges(){
		return new TreeMap<String, String>(ageList);
	}
	
	public Map<String, String> populateRegisteredBefore(){
		Map<String,String> registeredBefore = new LinkedHashMap<String,String>();
		registeredBefore.put("true", "yes");
		registeredBefore.put("false", "no");
		return registeredBefore;
	}
	public Map<String, String> populateOptionalServices(){
		Map<String,String> optionalServices = new LinkedHashMap<String,String>();
		optionalServices.put("emailService", "Mailing List");
		optionalServices.put("promotionalService", "Promotional Emails");
		optionalServices.put("newsLetterService", "Weekly Newsletter");
		return optionalServices;
	}
	public Map<String, String> populatePremiumServices(){
		Map<String,String> premiumServices = new LinkedHashMap<String,String>();
		premiumServices.put("directoryService", "Directory Service");
		premiumServices.put("revenueReportService", "Revenue Reports");
		return premiumServices;
	}
	public Map<String, String> populateEmployeeStrength(){
		Map<String,String> employeeStrength = new LinkedHashMap<String,String>();
		employeeStrength.put("small", "less than 100");
		employeeStrength.put("large", "more than 1000");
		return employeeStrength;
	}
}
