package org.honor.springmvc.javaconfig.example1.validation.test;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AgeConstraintValidator implements ConstraintValidator<AgeConstraint, Integer> {
	private int lower;
	private int upper;
	
	@Override
	public void initialize(AgeConstraint constraintAnnotation) {
		lower = constraintAnnotation.lower();
		upper = constraintAnnotation.upper();
	}
	/*
	 * context is a required argument even if it is not used!
	 * */
	@Override
	public boolean isValid(Integer age, ConstraintValidatorContext context) {
		if (age==null) return false;
		if (age<lower || age>upper) return false;
		return true;
	}
	
}
