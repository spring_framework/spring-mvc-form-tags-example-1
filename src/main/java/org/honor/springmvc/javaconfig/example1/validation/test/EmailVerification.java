package org.honor.springmvc.javaconfig.example1.validation.test;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target(FIELD)
@Constraint(validatedBy=EmailVerificationValidator.class)
public @interface EmailVerification {
	String message() default "email is not valid";
	
	//we won't be using this but it needs to be here to be a valid constraint
	Class<?>[] groups() default {};
	
	//we won't be using this but it needs to be here to be a valid constraint
	Class<? extends Payload>[] payload() default {};
}
