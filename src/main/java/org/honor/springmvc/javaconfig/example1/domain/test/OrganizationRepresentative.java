package org.honor.springmvc.javaconfig.example1.domain.test;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.honor.springmvc.javaconfig.example1.validation.test.AgeConstraint;
import org.honor.springmvc.javaconfig.example1.validation.test.EmailVerification;
import org.honor.springmvc.javaconfig.example1.validation.test.FieldsVerification;

@FieldsVerification.List({
	@FieldsVerification(
		field = "email",
		fieldMatch = "verifyEmail",
		message = "email addresses don't match"
	),
	@FieldsVerification(
			field = "password",
			fieldMatch = "verifyPassword",
			message = "passwords don't match"
	)
})
public class OrganizationRepresentative {
	
	@NotBlank(message=" first name cannot be blank!")
	private String firstName;
	
	@NotEmpty(message=" surname cannot be empty!")
	@Size(min=3, max=10, message=" min 3 chars max 10 chars should be entered")
	private String lastName;
	
	@AgeConstraint(lower=20, message="age must be between 20-60")//override default value for "lower"
	private Integer age;
	
	@NotEmpty(message="zipcode cannot be empty")
	@Pattern(regexp="[a-zA-Z0-9]{6}", message="6 chars or digits only")
	private String zipCode;
	
	@EmailVerification(message="email is invalid")
	@NotBlank(message=" email cannot be blank!")
	private String email;
	
	@NotBlank(message=" email cannot be blank!")
	private String verifyEmail;
	
	@NotBlank(message=" password cannot be blank!")
	private String password;
	
	@NotBlank(message=" password cannot be blank!")
	private String verifyPassword;
	
	public String getVerifyEmail() {
		return verifyEmail;
	}

	public void setVerifyEmail(String verifyEmail) {
		this.verifyEmail = verifyEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVerifyPassword() {
		return verifyPassword;
	}

	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
}
