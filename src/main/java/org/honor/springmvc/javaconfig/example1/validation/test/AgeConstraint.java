package org.honor.springmvc.javaconfig.example1.validation.test;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target(FIELD)
@Constraint(validatedBy=AgeConstraintValidator.class)
public @interface AgeConstraint {
	
	/*
	 * "message" attribute
	 * default error message should be provided or u will get a runtime error
	 * */
	String message() default "age must be between 18-60";
	
	//we won't be using this but it needs to be here to be a valid constraint
	Class<?>[] groups() default {};
	
	//we won't be using this but it needs to be here to be a valid constraint
	Class<? extends Payload>[] payload() default {};
	
	//"lower" attribute
	int lower() default 18;
	
	//"upper" attribute
	int upper() default 60;
}
