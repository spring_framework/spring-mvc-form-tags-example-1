package org.honor.springmvc.javaconfig.example1.controller;

import java.util.Map;

import org.honor.springmvc.javaconfig.example1.domain.test.OrganizationRegistration;
import org.honor.springmvc.javaconfig.example1.service.test.OrganizationRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/formTagsDemo")
public class FormTagsController {
	
	@Autowired
	OrganizationRegistrationService orgRegService;

	@RequestMapping("/home")
	public ModelAndView home(Model model) {
		return new ModelAndView("test/formTagsTestViews/formTagsHome", "orgRegistration",
				new OrganizationRegistration());
	}

	@RequestMapping(value = "/registerOrg", method = RequestMethod.POST)
	public String organizationRegistration(@ModelAttribute OrganizationRegistration orgRegistration, Model model) {
		model.addAttribute("orgreg", orgRegistration);
		return "test/formTagsTestViews/formTagsResult";
	}
	
	@ModelAttribute
	public void populateFormObject(Model model) {
		Map <String, Object> map = model.asMap();
		map.put("turnoverlist", orgRegService.populateTurnover());
		map.put("typelist", orgRegService.populateTypes());
		map.put("agelist", orgRegService.populateServiceAges());
		map.put("registeredbeforelist", orgRegService.populateRegisteredBefore());
		map.put("subscriptionlist", orgRegService.populateOptionalServices());
		map.put("premiumlist", orgRegService.populatePremiumServices());
		map.put("employeestrengthlist", orgRegService.populateEmployeeStrength());
	}
	
}
