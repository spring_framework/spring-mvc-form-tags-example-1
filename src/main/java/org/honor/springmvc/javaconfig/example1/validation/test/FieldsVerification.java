package org.honor.springmvc.javaconfig.example1.validation.test;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy=FieldsVerificationValidator.class)
public @interface FieldsVerification {
	String message() default "field values don't match";
	String field();
	String fieldMatch();
	
	//we won't be using this but it needs to be here to be a valid constraint
	Class<?>[] groups() default {};
	
	//we won't be using this but it needs to be here to be a valid constraint
	Class<? extends Payload>[] payload() default {};
	
	@Retention(RUNTIME)
	@Target(ElementType.TYPE)
	@interface List{
		FieldsVerification[] value();
	}
}
