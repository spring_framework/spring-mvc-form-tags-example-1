package org.honor.springmvc.javaconfig.example1.controller.test;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "/requestMappingParamExample")
@Controller
public class RequestMappingParamExampleController {
	private static Logger LOGGER = LoggerFactory.getLogger(RequestMappingParamExampleController.class);

	@RequestMapping(value = "/home")
	public String home() {
		return "requestMappingParamHome";
	}

	// @RequestParam without explicit attributes
	@RequestMapping(value = "/test1")
	public String requestMappingParamTest1(@RequestParam("orgname") String orgName, Model model) {
		model.addAttribute("orgname", orgName);
		model.addAttribute("testmethod", "test1");
		return "requestMappingParamResult";
	}

	// test @RequestMapping 'method' attribute
	// this handler method will only hanle requests of type GET
	@RequestMapping(value = "/test2", method = RequestMethod.GET)
	public String requestMappingParamTest2(@RequestParam("orgname") String orgName, Model model) {
		model.addAttribute("orgname", orgName);
		model.addAttribute("testmethod", "test2");
		return "requestMappingParamResult";
	}

	// test3: @RequestMapping fallback feature
	// for any non-existing request mapping, this view will be presented
	@RequestMapping(value = "*", method = RequestMethod.GET)
	public String requestMappingParamTest3() {
		return "fallback";
	}

	// test4: @RequestParam 'defaultValue' attribute
	@RequestMapping(value = "/test4")
	public String requestMappingParamTest4(
			@RequestParam(value = "orgname", defaultValue = "anonymous org.") String orgName, Model model) {
		model.addAttribute("orgname", orgName);
		model.addAttribute("testmethod", "test4");
		return "requestMappingParamResult";
	}

	// test5: @RequestParam without 'name' or value' attributes
	// in this case the name of the methode argument should be exactly the same as
	// the request parameter's name!
	@RequestMapping(value = "/test5", method = RequestMethod.GET)
	public String requestMappingParamTest5(@RequestParam String orgname, Model model) {
		model.addAttribute("orgname", orgname);
		model.addAttribute("testmethod", "test5");
		return "requestMappingParamResult";
	}

	// Test6 subtest1 - Testing removal of request mapping ambiguity for same base
	// uri with different a different parameter
	@RequestMapping(value = "/test6", params = "orgname")
	public String requestMappingParamTest6Subtest1(@RequestParam String orgname, Model model) {
		model.addAttribute("orgname", orgname);
		model.addAttribute("testmethod", "test6-subtest1");
		return "requestMappingParamResult2";
	}

	// Test6 subtest2 - Testing removal of request mapping ambiguity for same base
	// uri with different a different parameter
	@RequestMapping(value = "/test6", params = "empcount")
	public String requestMappingParamTest6Subtest2(@RequestParam String empcount, Model model) {
		model.addAttribute("orgname", empcount);
		model.addAttribute("testmethod", "test6-subtest2");
		return "requestMappingParamResult2";
	}

	// Test6 subtest3 - Testing request mapping with multiple request params
	@RequestMapping(value = "/test6/subtest3", method = RequestMethod.GET, params = { "empcount", "orgname" })
	public String requestMappingParamTest6Subtest3(@RequestParam String orgname, @RequestParam String empcount,
			Model model) {
		model.addAttribute("orgname", orgname);
		model.addAttribute("empcount", empcount);
		model.addAttribute("testmethod", "test6-subtest3");
		return "requestMappingParamResult2";
	}

	// Test6 subtest4 - Testing multiple request params and @RequestParam with single parameter
	@RequestMapping(value = "/test6/subtest4", method = RequestMethod.GET, params = { "empcount", "orgname" })
	public String requestMappingParamTest6Subtest4(@RequestParam String orgname, Model model) {
		model.addAttribute("orgname", orgname);
		model.addAttribute("testmethod", "test6-subtest4");
		return "requestMappingParamResult2";
	}
	
	// Test 7 and 8 - Testing @RequestMapping with multiple URIs
	@RequestMapping(value = {"/test7", "/test8"}, method = RequestMethod.GET)
	public String requestMappingParamTest78(@RequestParam String orgname, Model model, HttpServletRequest request) {
		model.addAttribute("orgname", orgname);
		LOGGER.info(request.getRequestURL().toString());
		if(request.getRequestURL().toString().contains("/test7")) {
			model.addAttribute("testmethod", "test7");
		}else {
			model.addAttribute("testmethod", "test8");
		}
		return "requestMappingParamResult2";
	}
}
