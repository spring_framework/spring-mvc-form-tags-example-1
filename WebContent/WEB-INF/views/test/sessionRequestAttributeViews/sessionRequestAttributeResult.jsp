<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<title>Test Results</title>
<spring:url value="/resource/test-main.css" var="testMainCSS" />
<link href="${testMainCSS}" rel="stylesheet" />
</head>
<body>
<div>
<h1>@SessionAttributes test results</h1>
<h3>${timeHeading }, ${durationHeading }</h3>
<hr>
<h3>Number of page visits in this session: <c:out value="${visitorcount.count}" /></h3>
<h3>List of visitors:</h3>
<ul>
<c:forEach var="visitor" items="${visitordata.visitors}">
<li><c:out value="${visitor.name}"></c:out>, <c:out value="${visitor.email}"></c:out></li>
</c:forEach>
</ul>
<br>
<br>
<c:set var ="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/visitorRegister/home">-->Generate another visit</a>
</div>
</body>
</html>