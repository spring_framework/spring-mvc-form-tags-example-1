<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<spring:url value="resources/test-main.css" var="estMainCSS" />
<link href="${testMainCSS}" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>testbed for spring form tags</title>
</head>
<body>
	<div align="center">
		<h1>welcome to the form tags testbed</h1>
		<hr>
		<h3>register your organization</h3>
		<form:form action="registerOrg" modelAttribute="orgRegistration">
			<table>
				<tr>
					<td><form:label path="orgName">Name of Organization</form:label></td>
					<td><form:input path="orgName"
							placeholder="organization name here"></form:input></td>
				</tr>
				<tr>
					<td><form:label path="country">country</form:label></td>
					<td><form:select path="country">
							<form:option value="none">-----select-----</form:option>
							<form:option value="Australia">Australia</form:option>
							<form:option value="India">India</form:option>
							<form:option value="USA">USA</form:option>
							<form:option value="Turkey">Turkey</form:option>
							<form:option value="Sweden">Sweden</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td><form:label path="turnover">Turnover</form:label></td>
					<td><form:select path="turnover" items="${turnoverlist}"></form:select></td>
				</tr>
				<tr>
					<td><form:label path="type">Type</form:label></td>
					<td><form:select path="type">
							<form:option value="none">-----select----</form:option>
							<form:options items="${typelist}"></form:options>
						</form:select></td>
				</tr>
				<tr>
					<td><form:label path="age">Organization Age</form:label></td>
					<td><form:select path="age" items="${agelist}"></form:select></td>
				</tr>
				<tr>
					<td><form:label path="registeredBefore">Registered Before</form:label></td>
					<td><form:checkbox path="registeredBefore"
							items="${registeredbeforelist}"></form:checkbox></td>
				</tr>
				<tr>
					<td><form:label path="optionalServices">Optional Services Registration:</form:label></td>
					<td><form:checkbox path="optionalServices"
							value="emailService" />${subscriptionlist.emailService} <form:checkbox
							path="optionalServices" value="promotionalService" />${subscriptionlist.promotionalService}
						<form:checkbox path="optionalServices" value="newsLetterService" />${subscriptionlist.newsLetterService}
					</td>
				</tr>
				<tr>
					<td><form:label path="premiumServices">Premium Services:</form:label></td>
					<td><form:checkboxes path="premiumServices"
							items="${premiumlist}"></form:checkboxes></td>
				</tr>
				<tr>
					<td><form:label path="overseasOperations">Overseas Operations?</form:label></td>
					<td><form:radiobutton path="overseasOperations" value="yes" />Yes
						<form:radiobutton path="overseasOperations" value="no" />No</td>
				</tr>
				<tr>
					<td><form:label path="employeeStrength">Employee Size</form:label></td>
					<td><form:radiobuttons path="employeeStrength" 
							items="${employeestrengthlist }" />
				</tr>
				<tr>
					<td><form:label path="like">Like this page?</form:label></td>
					<td><form:checkbox path="like" value="yes" />Like</td>
				</tr>
				<tr>
					<td></td>
					<td align="center"><input type="submit" value="register" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>