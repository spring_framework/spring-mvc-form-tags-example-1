<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<spring:url value="resources/test-main.css" var="estMainCSS" />
<link href="${testMainCSS}" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>form tags test result</title>
</head>
<body>
	<h1>congratulations you have registered successfully!</h1>
	<h2>Details below</h2>
	<hr>
	<div>
		<h3>Organization Name: ${orgreg.orgName}</h3>
		<h3>Organization Country: ${orgreg.country}</h3>

		<h3>
			Turnover:
			<c:forEach var="entry" items="${turnoverlist}">
				<c:if test="${orgreg.turnover eq entry.key}">
			${entry.value}
		</c:if>
			</c:forEach>
		</h3>
		<h3>
			Type:
			<c:forEach var="entry" items="${typelist}">
				<c:if test="${orgreg.type eq entry.key}">
			${entry.value}
		</c:if>
			</c:forEach>

		</h3>

		<h3>
			Organization Age:
			<c:forEach var="entry" items="${agelist}">
				<c:if test="${orgreg.age eq entry.key}">
			${entry.value}
		</c:if>
			</c:forEach>

		</h3>

		<h3>
			Registered Before:
			<c:forEach var="entry" items="${registeredbeforelist}">
				<c:if test="${orgreg.registeredBefore eq entry.key}">
			${entry.value}
		</c:if>
			</c:forEach>

		</h3>


		<h3>
			Optional Services Signed Up:
			<c:forEach var="entry1" items="${subscriptionlist}">
				<c:forEach var="entry2" items="${orgreg.optionalServices}">
					<c:if test="${entry2 eq entry1.key}">
						<c:set var="optServices" scope="request"
							value="${optServices}${entry1.value}, " />
					</c:if>
				</c:forEach>
			</c:forEach>
			<b>${optServices.substring(0, optServices.length()-2)}</b>
		</h3>

		<h3>
			Premium Services Signed Up:
			<c:forEach var="entry1" items="${premiumlist}">
				<c:forEach var="entry2" items="${orgreg.premiumServices}">
					<c:if test="${entry2 eq entry1.key}">
						<c:set var="preServices" scope="request"
							value="${preServices}${entry1.value}, " />
					</c:if>
				</c:forEach>
			</c:forEach>
			<b>${preServices.substring(0, preServices.length()-2)}</b>
		</h3>
		<h3>Has Overseas Operations? ${orgreg.overseasOperations}</h3>
		<h3>
			Workforce Size:
			<c:forEach var="entry" items="${employeestrengthlist}">
				<c:if test="${orgreg.employeeStrength eq entry.key}">
			${entry.value}
		</c:if>
			</c:forEach>

		</h3>
		<h3>
			Like this page?:
			<c:choose>
				<c:when test="${orgreg.like eq 'yes'}">Like</c:when>
				<c:otherwise>don't like</c:otherwise>
			</c:choose>
		</h3>
	</div>
</body>
</html>