<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<spring:url value="/resources/test-main.css" var="testMainCSS" />
<link href="${testMainCSS}" rel="stylesheet" />
<title>Validation Test Bed</title>
</head>
<body>
	<div align="center">
		<h1 class="ch1">form validation test bed</h1>
		<hr />
		<h3>register as organization representative</h3>
		<form:form action="registerOrgRep" modelAttribute="orgrep">
			<table>
				<tr>
					<td><form:label path="firstName" cssClass="clabel">First Name</form:label></td>
					<td><form:input path="firstName"
							placeholder="Enter First Name" cssClass="cinput" /></td>
					<td><form:errors path="firstName" cssClass="cb" /></td>
				</tr>
				<tr>
					<td><form:label path="lastName" cssClass="clabel">Last Name</form:label></td>
					<td><form:input path="lastName" placeholder="Enter Surname"
							cssClass="cinput" /></td>
					<td><form:errors path="lastName" cssClass="cb" /></td>
				</tr>
				<tr>
					<td><form:label path="age" cssClass="clabel">Age</form:label></td>
					<td><form:input path="age" placeholder="Age" cssClass="cinput" /></td>
					<td><form:errors path="age" cssClass="cb" /></td>
				</tr>
				<tr>
					<td><form:label path="zipCode" cssClass="clabel">Zip Code</form:label></td>
					<td><form:input path="zipCode" placeholder="Enter ZipCode"
							cssClass="cinput" /></td>
					<td><form:errors path="zipCode" cssClass="cb" /></td>
				</tr>
				<tr>
					<td><form:label path="email" cssClass="clabel">eMail</form:label></td>
					<td><form:input path="email" placeholder="Enter eMail"
							cssClass="cinput" /></td>
					<td class="cb"><form:errors path="email" /> <spring:hasBindErrors
							name="orgrep">
							<!-- hasBindErrors is used for catching global errors like class level validation errors  -->
							<c:forEach items="${errors.globalErrors}" var="globalError">
								<c:if test="${fn:contains(globalError, 'email')}">
									<!-- fn:contains is a JSTL function. loop and check if any global error is related to email validation -->
									<c:out value="${globalError.defaultMessage}" />
								</c:if>
							</c:forEach>
						</spring:hasBindErrors></td>
				</tr>
				<tr>
					<td><form:label path="verifyEmail" cssClass="clabel">Verify eMail</form:label></td>
					<td><form:input path="verifyEmail" placeholder="Verify eMail"
							cssClass="cinput" /></td>
					<td><form:errors path="verifyEmail" cssClass="cb"/></td>
				</tr>
				<tr>
					<td><form:label path="password" cssClass="clabel">password</form:label></td>
					<td><form:password path="password"
							placeholder="Enter password" cssClass="cinput" /></td>
					<td class="cb" ><form:errors path="password" /> <spring:hasBindErrors
							name="orgrep">
							<!-- hasBindErrors is used for catching global errors like class level validation errors  -->
							<c:forEach items="${errors.globalErrors}" var="globalError">
								<c:if test="${fn:contains(globalError, 'password')}">
									<!-- fn:contains is a JSTL function. loop and check if any global error is related to email validation -->
									<c:out value="${globalError.defaultMessage}" />
								</c:if>
							</c:forEach>
						</spring:hasBindErrors></td>
				</tr>
				<tr>
					<td><form:label path="verifyPassword" cssClass="clabel">Verify password</form:label></td>
					<td><form:password path="verifyPassword"
							placeholder="Verify password" cssClass="cinput" /></td>
					<td><form:errors path="verifyPassword" cssClass="cb" /></td>
				</tr>
				<tr>
					<td />
					<td align="center"><input type="submit" value="Submit"
						class="csubmit" /></td>
				</tr>
			</table>
			<hr />
			<c:set var="contextPath" value="${pageContext.request.contextPath}" />
			<c:if test="${formerrors ne null}">
				<a href="${contextPath}/formValidationDemo/home"
					style="font-size: 17px">Click here to start a fresh form!</a>
			</c:if>
		</form:form>
	</div>
</body>
</html>