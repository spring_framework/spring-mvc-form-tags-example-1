<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>@ModelAttribute testbed.</title>
</head>
<body>
	<div align="center">
		<h1>${testData1A}</h1>
		<hr>
		${testData1B} ${testdata2}
		<hr>
		<div>
			<b>Office location: ${testdata3.city} ${testdata3.zipCode}</b>
		</div>
		<div>
			<b>Corporate office: ${address.city} ${address.zipCode}</b>
		</div>
	</div>
	<h3>Test5: testing the @ModelAttribute annotation with the value attribute and default binding</h3>
	<form:form action="test5" modelAttribute="anAddress">
		<table>
			<tr>
				<td><form:label path="city">City Name</form:label></td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td><form:label path="zipCode">Postal Code</form:label></td>
				<td><form:input path="zipCode" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Display Address" /></td>
			</tr>
		</table>
	</form:form>
	<br><br>
	<form:form action="modelAttributeTest">
		<h3>testing @ModelAttribute with no explicit logical view name</h3>
		<input type="submit" value="relocate" />
	</form:form>
</body>
</html>